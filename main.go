package main

import (
	"clean-code/app/handler"
	"clean-code/app/repository/database"
	"clean-code/app/usecase/articles"
	pkg "clean-code/pkg/helper"
	"fmt"
)

func main() {

	conn, err := pkg.Connection()

	if err != nil {
		panic(err.Error())
	}

	repo := database.NewArticles(conn)

	uscase := articles.NewUscase(repo)

	getArticles := handler.NewArticles(uscase)

	fmt.Println(getArticles.GetAllUser())

	var article string

	if article, err = getArticles.GetByID(4); err != nil {
		fmt.Println(err.Error())
	}

	fmt.Println(article)

}
