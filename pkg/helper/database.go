package helper

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func Connection() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:smartfren@tcp(127.0.0.1:3306)/article")

	db.SetMaxOpenConns(100)
	if err != nil {
		return nil, err
	}

	return db, nil
}
