package articles

import (
	"clean-code/app/repository"
	"clean-code/app/usecase"
)

type Uscase struct {
	Repo repository.RepositoryArticles
}

func NewUscase(uscase repository.RepositoryArticles) usecase.UCaseArticles {
	return &Uscase{Repo: uscase}
}

func (u Uscase) GetAllArticles() (str string, err error) {
	if str, err = u.Repo.GetAllArticles(); err != nil {
		return "", err
	}

	if str == "" {
		return "data not found", nil
	}
	return str, nil
}

func (u Uscase) GetByID(id int) (str string, err error) {

	if str, err = u.Repo.GetByIDArticles(id); err != nil {
		return "", err
	}

	if str == "" {
		return "Data not found", nil
	}
	return str, nil
}
