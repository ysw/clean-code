package usecase

type UCaseArticles interface {
	GetAllArticles() (string, error)
	GetByID(id int) (string, error)
}
