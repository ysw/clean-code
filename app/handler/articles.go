package handler

import (
	"clean-code/app/usecase"
)

type Articles struct {
	Articles usecase.UCaseArticles
}

func NewArticles(articles usecase.UCaseArticles) *Articles {
	return &Articles{Articles: articles}
}

func (a Articles) GetAllArticles() (str string, err error) {
	if str, err = a.Articles.GetAllArticles(); err != nil {
		return "", err
	}
	return str, nil
}

func (a Articles) GetByID(id int) (str string, err error) {
	if str, err = a.Articles.GetByID(id); err != nil {
		return "", err
	}
	return str, nil
}
