package repository

type RepositoryArticles interface {
	GetAllArticles() (string, error)
	GetByIDArticles(id int) (string, error)
}
