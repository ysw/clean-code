package database

import (
	"clean-code/app/entity"
	"clean-code/app/repository"
	"database/sql"
	"fmt"
)

type Articles struct {
	Db *sql.DB
}

func NewArticles(db *sql.DB) repository.RepositoryArticles {
	return &Articles{Db: db}
}

func (a Articles) GetByIDArticles(id int) (str string, err error) {

	query := fmt.Sprintf("%s %v", "SELECT id,title FROM article where id = ", id)
	results, err := a.Db.Query(query)
	if err != nil {
		return "", err
	}
	for results.Next() {
		var order entity.Articles
		err = results.Scan(&order.Id, &order.Title)
		if err != nil {
			return "", err
		}
		str += fmt.Sprintf("%v-%s", order.Id, order.Title)
	}

	defer results.Close()

	return str, nil
}

func (a Articles) GetAllArticles() (str string, err error) {

	results, err := a.Db.Query("SELECT id,title FROM article")
	if err != nil {
		return "", err
	}
	for results.Next() {
		var order entity.Articles
		err = results.Scan(&order.Id, &order.Title)
		if err != nil {
			return "", err
		}
		str += fmt.Sprintf("%v-%s", order.Id, order.Title)
	}

	defer results.Close()

	return str, nil
}
